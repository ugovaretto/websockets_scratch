#include <cstring>
#include <cassert>
#include <iostream>
#include <libwebsockets.h>


using namespace std;

static volatile int force_exit = 0;

#define LOCAL_RESOURCE_PATH

#define MAX_ECHO_PAYLOAD 1024

struct per_session_data__echo {
    size_t rx, tx;
    unsigned char buf[LWS_PRE + MAX_ECHO_PAYLOAD];
    unsigned int len;
    unsigned int index;
    int final;
    int continuation;
    int binary;
    int initialized;
};

static int
callback_echo(struct lws *wsi, enum lws_callback_reasons reason, void *user,
              void *in, size_t len)
{
    struct per_session_data__echo *pss =
        (struct per_session_data__echo *)user;
    int n;

    switch (reason) {
    case LWS_CALLBACK_SERVER_WRITEABLE:
        n = LWS_WRITE_CONTINUATION;
        if (!pss->continuation) {
            //if (pss->binary)
                n = LWS_WRITE_BINARY;
            //else
            //    n = LWS_WRITE_TEXT;
            pss->continuation = 1;
        }
        if (!pss->final)
            n |= LWS_WRITE_NO_FIN;

        pss->tx += pss->len;
        n = lws_write(wsi, &pss->buf[LWS_PRE], pss->len, lws_write_protocol(n));
        cout << "SENT " << n << endl;
        if (n < 0) {
            lwsl_err("ERROR %d writing to socket, hanging up\n", n);
            return 1;
        }
        if (n < (int)pss->len) {
            lwsl_err("Partial write\n");
            return -1;
        }
        pss->len = -1;
        if (pss->final)
            pss->continuation = 0;
        lws_rx_flow_control(wsi, 1); //re-start accepting data
        break;
    case LWS_CALLBACK_RECEIVE:
        pss->final = lws_is_final_fragment(wsi);
        pss->binary = lws_frame_is_binary(wsi);
        lwsl_info("+++ test-echo: RX len %d final %d, pss->len=%d\n",
                  len, pss->final, (int)pss->len);

        memcpy(&pss->buf[LWS_PRE], in, len);

        if(pss->initialized)
            assert((int)pss->len == -1);
        else {
            assert(pss->len == 0);
            pss->initialized = 1;
        }
        pss->len = (unsigned int)len;
        pss->rx += len;
        cout << pss->final << endl
             << pss->binary << endl
             << pss->initialized << endl
             << pss->len << endl
             << pss->len << endl
             << pss->rx << endl;
        cout << (char*) pss->buf << endl;
        memmove(pss->buf, in, len * sizeof(unsigned char));
        lws_rx_flow_control(wsi, 0); //stop accepting data until message is
                                     //sent back
        lws_callback_on_writable(wsi);
        break;
    default:
        break;
    }

    return 0;
}



static struct lws_protocols protocols[] = {
    /* first protocol must always be HTTP handler */

    {
        "echo",		/* name - can be overriden with -e */
        callback_echo,
        sizeof(struct per_session_data__echo),	/* per_session_data_size */
        MAX_ECHO_PAYLOAD,
    },
    {
        NULL, NULL, 0		/* End of list */
    }
};

void sighandler(int sig)
{
    force_exit = 1;
}

int main() {

    int n = 0;
    int port = 7681;
    struct lws_context *context;
    struct lws_context_creation_info info;
    int listen_port = 80;

    memset(&info, 0, sizeof info);

    lwsl_notice("libwebsockets test server echo - license LGPL2.1+SLE\n");
    lwsl_notice("(C) Copyright 2010-2016 Andy Green <andy@warmcat.com>\n");
    lwsl_notice("Running in server mode\n");
    listen_port = port;

    info.port = listen_port;
    info.iface = nullptr;
    info.protocols = protocols;

    info.gid = -1;
    info.uid = -1;

    context = lws_create_context(&info);
    if (context == NULL) {
        lwsl_err("libwebsocket init failed\n");
        return -1;
    }

    signal(SIGINT, sighandler);

    n = 0;
    while (n >= 0 && !force_exit) {
        n = lws_service(context, 10);
    }

    lws_context_destroy(context);

    lwsl_notice("libwebsockets-test-echo exited cleanly\n");

    return 0;
}