#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <limits>
#include <string>
#include <mutex>
#include <vector>
#include <fstream>
#include <thread>
#include <chrono>
#include <future>
#include <libwebsockets.h>

//Note: use libev if possible

using namespace std;

namespace {
volatile bool forceExit = 0; //set by signal handler
}

class Data {
public:
    Data() : id_(0), frameTime_(30) /*ms*/ {}
    size_t ID() const {
        return id_;
    }
    void Set(const vector< unsigned char >& v) {
        lock_guard< mutex > l(guard_);
        data_ = v;
        ++id_;
        if(!id_) ++id_; //0 means empty
    }
    void Get(vector< unsigned char >& v) const {
        lock_guard< mutex > l(guard_);
        v.resize(LWS_PRE + data_.size());
        copy(data_.begin(), data_.end(), v.begin() + LWS_PRE);
    }
    bool Empty() const { return id_ == 0; }
    const chrono::milliseconds FrameTime() const { return frameTime_; }
private:
    size_t id_;
    vector< unsigned char > data_;
    chrono::milliseconds frameTime_;
    mutable mutex guard_;
};


struct PerSessionData {
    vector< unsigned char > buf;
    size_t lastId; //id of last data sent
    Data* data;    //reference to data
    chrono::steady_clock::time_point prev;
};

int
CallbackStream(lws *wsi,
               lws_callback_reasons reason,
               void* user,
               void* in,
               size_t len) {

    PerSessionData *pss = reinterpret_cast< PerSessionData* >(user);
    lws_context* context = lws_get_context(wsi);

    switch (reason) {
    case LWS_CALLBACK_SERVER_WRITEABLE: {
        Data* data = reinterpret_cast< Data* >(lws_context_user(context));
        pss->data = data;
        if(!pss->data
            || pss->data->Empty()
            || pss->lastId == pss->data->ID()) {
            lws_callback_on_writable(wsi);
            break;
        }
        pss->data->Get(pss->buf);
        pss->lastId = pss->data->ID();
        lws_write_protocol writeMode = LWS_WRITE_BINARY;
        const int sent =
            lws_write(wsi, &pss->buf[LWS_PRE], pss->buf.size() - LWS_PRE,
                      writeMode);
        if(sent < 0) {
            lwsl_err("ERROR %d writing to socket, hanging up\n", sent);
            return -1;
        }
        if(sent < pss->buf.size() - LWS_PRE) {
            lwsl_err("Partial write\n");
            return -1;
        }
        const chrono::milliseconds d =
            chrono::duration_cast< chrono::milliseconds >
                (chrono::steady_clock::now() - pss->prev);
        const auto ft = data->FrameTime();
        if(d < ft) this_thread::sleep_for(ft - d);
        pss->prev = chrono::steady_clock::now();
        lws_callback_on_writable(wsi);
    }
        break;
    case LWS_CALLBACK_ESTABLISHED:
        if(pss->lastId == 0) {
            new (&pss->buf) vector< unsigned char >;
            pss->prev = chrono::steady_clock::now();
        }
        lws_callback_on_writable(wsi);
        break;
    case LWS_CALLBACK_CLOSED:
        pss->buf.~vector< unsigned char >();
        break;
    default:
        break;
    }

    return 0;
}


lws_protocols protocols[] = {
    //if using http put the http handler as the first entry in the table
    {
        "image-stream",	//protocol name
        CallbackStream, //callback
        sizeof(PerSessionData),	// per session data size
        0x1000, //receive buffer size
        0, //ID
        nullptr
    },
    {
       nullptr, nullptr, 0, 0, 0, nullptr //END
    }
};

void sighandler(int) {
    forceExit = 1;
}

size_t FileSize(const string& fname) {
    ifstream file(fname);
    assert(file);
    file.ignore(std::numeric_limits<std::streamsize>::max());
    std::streamsize length = file.gcount();
    file.clear();   //  Since ignore will have set eof.
    //file.seekg( 0, std::ios_base::beg );
    return length;
}


int main(int argc, char** argv) {

    //--------------------------------------------------------------------------
    if(argc != 4) {
        cerr << "usage: " << argv[0] << " <prefix> <num images> <suffix>"
             << endl;
        cerr << "E.g.: '>image_stream frame 10 .jpg' loops over "
             << "frame0.jpg to frame9.jpg"
             << endl;
        return EXIT_FAILURE;
    }
    const string prefix = argv[1];
    const int numfiles = int(stol(argv[2]));
    const string suffix = argv[3];
    const int frameDigits = int(floor(log10(numfiles)));
    using Image = vector< unsigned char >;
    using ImageArray = vector< Image >;
    ImageArray imgs;
    for(int i = 0; i != numfiles; ++i) {
        const size_t padding =
            max(0, frameDigits - int(floor(log10(i + 1))) - 1);
        assert(padding >= 0);
        const string fname =
            prefix
            + string(padding, '0')
            + to_string(i)
            + suffix;
        ifstream is(fname, ios::binary);
        assert(is);
        const size_t fileSize = FileSize(fname);
        assert(fileSize);
        Image img(fileSize);
        is.read(reinterpret_cast< char* >(&img[0]), fileSize);
        imgs.push_back(img);
    }

    //--------------------------------------------------------------------------
    lwsl_notice("libwebsockets streaming server\n");
    lws_context* context;
    lws_context_creation_info info;
    int listenPort = 7681;
    Data data;

    memset(&info, 0, sizeof info);

    info.port = listenPort;
    info.iface = nullptr;
    info.protocols = protocols;
    info.user = &data;

    //warnings if following not set to something
    info.gid = -1;
    info.uid = -1;

    context = lws_create_context(&info);
    if (context == NULL) {
        lwsl_err("libwebsocket init failed\n");
        return EXIT_FAILURE;
    }

    signal(SIGINT, sighandler);


    data.Set(imgs.front());
    auto updateTask = async(launch::async,[&data, imgs]() {
        int count = 0;
        while(!forceExit) {
            data.Set(imgs[count % imgs.size()]);
            ++count;
            this_thread::sleep_for(chrono::milliseconds(28));
        }
    });
    while(!forceExit && lws_service(context, 1000) >=0);

    lws_context_destroy(context);

    lwsl_notice("libwebsockets streaming server exited cleanly\n");

    updateTask.get();

    //--------------------------------------------------------------------------
    return EXIT_SUCCESS;
}

//FRAGMENTED PACKETS:
//int n = libwebsocket_write(wsi, &write_buffer[LWS_SEND_BUFFER_PRE_PADDING],
//                           write_len,(libwebsocket_write_protocol)write_mode);
//The write_mode should be set as below:
//
//int write_mode;
//write_mode = LWS_WRITE_BINARY; // single frame, no fragmentation
//write_mode = LWS_WRITE_BINARY | LWS_WRITE_NO_FIN; // first fragment
//write_mode = LWS_WRITE_CONTINUATION | LWS_WRITE_NO_FIN;// all middle fragments
//write_mode = LWS_WRITE_CONTINUATION; // last fragment
//If you need more details, read the fragmentation section of the WebSocket
// RFC: https://tools.ietf.org/html/rfc6455#section-5.4