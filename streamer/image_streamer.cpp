#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <limits>
#include <string>
#include <mutex>
#include <vector>
#include <fstream>
#include <thread>
#include <chrono>
#include <future>
#include <algorithm>
#include <stdexcept>


#include <libwebsockets.h>

//Note: use libev if possible

///\todo replace Data with SyncQueue< Data >

//==============================================================================
class WSocketOutStream {
public:
    WSocketOutStream() = delete;
    WSocketOutStream(const WSocketOutStream&) = delete;
    WSocketOutStream(WSocketOutStream&&) = delete;
    WSocketOutStream(const std::string& protocolName,
                     int timeout,
                     int port) : stop(false) {
        InitConnection(protocolName, timeout, port);
    }
    ~WSocketOutStream() {
        Stop();
    }
    void Push(std::vector< unsigned char > d) {
        data.Set(d);
    }
private:
    class Data {
    public:
        Data() : id_(0), frameTime_(30) /*ms*/ {}
        size_t ID() const {
            return id_;
        }
        void Set(const std::vector< unsigned char >& v) {
            std::lock_guard< std::mutex > l(guard_);
            data_ = v;
            ++id_;
            if(!id_) ++id_; //0 means empty
        }
        void Get(std::vector< unsigned char >& v) const {
            std::lock_guard< std::mutex > l(guard_);
            v.resize(LWS_PRE + data_.size());
            std::copy(data_.begin(), data_.end(), v.begin() + LWS_PRE);
        }
        bool Empty() const { return id_ == 0; }
        const std::chrono::milliseconds FrameTime() const { return frameTime_; }
    private:
        size_t id_;
        std::vector< unsigned char > data_;
        std::chrono::milliseconds frameTime_;
        mutable std::mutex guard_;
    };

    struct PerSessionData {
        std::vector< unsigned char > buf;
        size_t lastId; //id of last data sent
        Data* data;    //reference to data
        std::chrono::steady_clock::time_point prev;
    };
private:
    void InitConnection(const std::string& protocol,
                        int timeout,
                        int port) {
        protocols = {
            //if using http put the http handler as the first entry in the table
            {
                protocol.c_str(),	//protocol name
                WSocketOutStream::CallbackStream, //callback
                sizeof(PerSessionData),	// per session data size
                0x1000, //receive buffer size
                0, //ID
                nullptr
            },
            {
                nullptr, nullptr, 0, 0, 0, nullptr //END
            }
        };
        memset(&info, 0, sizeof info);
        info.port = port;
        info.iface = nullptr;

        info.protocols = &protocols[0];
        info.user = this;

        //warnings if following not set to something
        info.gid = -1;
        info.uid = -1;
        context = lws_create_context(&info);
        wsocketsTask = std::async(std::launch::async, [this, timeout, port]() {
            while(!this->stop && lws_service(this->context, timeout) >=0);
        });
    }
    void Stop() {
        stop = true;
        wsocketsTask.get();
    }
private:
    lws_context* context;
    lws_context_creation_info info;
    Data data;
    bool stop;
    std::future< void > wsocketsTask;
    std::vector< lws_protocols > protocols;
private:
    static int
    CallbackStream(lws *wsi,
                   lws_callback_reasons reason,
                   void* user,
                   void* in,
                   size_t len) {

        PerSessionData *pss = reinterpret_cast< PerSessionData* >(user);
        lws_context* context = lws_get_context(wsi);

        switch (reason) {
        case LWS_CALLBACK_SERVER_WRITEABLE: {
            WSocketOutStream* wso =
                reinterpret_cast< WSocketOutStream* >(
                    lws_context_user(context));
            Data* data = &wso->data;
            pss->data = data;
            if(!pss->data
                || pss->data->Empty()
                || pss->lastId == pss->data->ID()) {
                lws_callback_on_writable(wsi);
                break;
            }
            pss->data->Get(pss->buf);
            pss->lastId = pss->data->ID();
            lws_write_protocol writeMode = LWS_WRITE_BINARY;
            const int sent =
                lws_write(wsi, &pss->buf[LWS_PRE], pss->buf.size() - LWS_PRE,
                          writeMode);
            if(sent < 0) {
                lwsl_err("ERROR %d writing to socket, hanging up\n", sent);
                return -1;
            }
            if(sent < pss->buf.size() - LWS_PRE) {
                lwsl_err("Partial write\n");
                return -1;
            }
            const std::chrono::milliseconds d =
                std::chrono::duration_cast< std::chrono::milliseconds >
                    (std::chrono::steady_clock::now() - pss->prev);
            const auto ft = data->FrameTime();
            if(d < ft) std::this_thread::sleep_for(ft - d);
            pss->prev = std::chrono::steady_clock::now();
            lws_callback_on_writable(wsi);
        }
            break;
        case LWS_CALLBACK_ESTABLISHED:
            if(pss->lastId == 0) {
                new (&pss->buf) std::vector< unsigned char >;
                pss->prev = std::chrono::steady_clock::now();
            }
            lws_callback_on_writable(wsi);
            break;
        case LWS_CALLBACK_CLOSED:
            pss->buf.~vector< unsigned char >();
            break;
        default:
            break;
        }

        return 0;
    }
};
//==============================================================================

namespace {
volatile bool forceExit = false;
}

void forceQuit(int) {
    forceExit = true;
}

using namespace std;

size_t FileSize(const string& fname) {
    ifstream file(fname);
    assert(file);
    file.ignore(std::numeric_limits<std::streamsize>::max());
    std::streamsize length = file.gcount();
    file.clear();   //  Since ignore will have set eof.
    //file.seekg( 0, std::ios_base::beg );
    return length;
}

int main(int argc, char** argv) {

    //--------------------------------------------------------------------------
    if(argc != 4) {
        cerr << "usage: " << argv[0] << " <prefix> <num images> <suffix>"
             << endl;
        cerr << "E.g.: '>image_stream frame 10 .jpg' loops over "
             << "frame0.jpg to frame9.jpg"
             << endl;
        return EXIT_FAILURE;
    }
    const string prefix = argv[1];
    const int numfiles = int(stol(argv[2]));
    const string suffix = argv[3];
    const int frameDigits = int(floor(log10(numfiles)));
    using Image = vector< unsigned char >;
    using ImageArray = vector< Image >;
    ImageArray imgs;
    for(int i = 0; i != numfiles; ++i) {
        const size_t padding =
            max(0, frameDigits - int(floor(log10(i + 1))) - 1);
        assert(padding >= 0);
        const string fname =
            prefix
            + string(padding, '0')
            + to_string(i)
            + suffix;
        ifstream is(fname, ios::binary);
        assert(is);
        const size_t fileSize = FileSize(fname);
        assert(fileSize);
        Image img(fileSize);
        is.read(reinterpret_cast< char* >(&img[0]), fileSize);
        imgs.push_back(img);
    }
    //
    WSocketOutStream wso("image-stream", //protocol name
                         1000, //timeout: will spend this time to process
                               //websocket traffic, the higher the better
                         7681);//port
    wso.Push(imgs.front());
    signal(SIGINT, forceQuit);
    int count = 0;
    while(!forceExit) {
        wso.Push(imgs[count % imgs.size()]);
        ++count;
        this_thread::sleep_for(chrono::milliseconds(28));
    }
    return EXIT_SUCCESS;
}

//sample javascript code, includes event handlers - remove or include
//event-handlers.js
/*******************************************************************************
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <script type="text/javascript" src="event-handlers.js">
    </script>
<script src=
    "http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function() {
    //CHANGE ACCORDINGLY
    var MIMETYPE = "image/jpeg";
    //var MIMETYPE = "image/webp";
    var websocket;
    window.WebSocket = window.WebSocket || window.MozWebSocket;
    var out = $("[name='output']");
    var bytes = $("[name='bytes']");
    var imageWidth  = $("[name='width']");
    var imageHeight  = $("[name='height']");
    var m  = $("[name='min']");
    var M  = $("[name='max']");
    var D  = $("[name='delta']");
    var img = document.querySelector( "#photo" );
    var blob = new Blob( [], { type: MIMETYPE } );
    var urlCreator = window.URL || window.webkitURL;
    var imageUrl;
    var b = document.querySelector( "body" );
    var W, H;
    var minSize = 0x100000000;
    var maxSize = 0;

    var proto = location.protocol;
    var hostname = "localhost";
    if(proto != "file:") hostname = location.hostname;
    var WSURL = "ws://" + hostname + ":7681";
    websocket = new WebSocket(WSURL, 'image-stream');
    websocket.binaryType = "arraybuffer";

    function resizeImage() {
        sendResizeEvent(window.innerWidth, window.innerHeight);
    }

    window.addEventListener('resize',resizeImage, true);

    while(eventScriptLoaded == undefined);
    initEvents(img, websocket);

    websocket.onopen = function () {
        $('h1').css('color', 'green');
    };

    websocket.onerror = function (e) {
        $('h1').css('color', 'red');
        console.log(e);
    };
    var start = performance.now();
    var elapsed = 0;
    var frames = 0;
    img.onload = function() {
        W = this.width;
        H = this.height;
        if(frames == 1) {
            resizeImage();
        }
    }
    var size;
    var dSize;

    websocket.onmessage = function (e) {
        if(imageUrl)
            urlCreator.revokeObjectURL(imageUrl);
        frames++;
        elapsed = performance.now() - start;
        //out.text(e.data.byteLength);
        out.text((1000 * frames / elapsed).toFixed(0));
        size = e.data.byteLength;
        bytes.text(size);
        if(size < minSize) minSize = size;
        if(size > maxSize) maxSize = size;
        dSize = maxSize - minSize;
        m.text(minSize);
        M.text(maxSize);
        D.text(dSize);
        imageWidth.text(W);
        imageHeight.text(H);
        blob = new Blob( [e.data], { type: MIMETYPE } );
        imageUrl = urlCreator.createObjectURL( blob );
        img.src = imageUrl;
    }

});
</script>
<style>
    body{
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
</head>
<body>
<div><img id="photo" style="pointer-events: none; position: absolute; top:0;left:0; overflow: scroll"/></div>
<div style="color: red; font-size: 200%; position: fixed">
    <h1>WebSockets stream test</h1>
<div>Frame/s: <span name="output"></span></div>
<div>Size: <span name="bytes"></span> bytes</div>
<div>Width: <span name="width"></span></div>
<div>Height: <span name="height"></span></div>
<div>Min size: <span name="min"></span></div>
<div>Max size: <span name="max"></span></div>
<div>Max - min size: <span name="delta"></span></div>
<div><span id="pos"></span></div>
</div>
</body>
</html>
*******************************************************************************/