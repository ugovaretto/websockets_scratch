#pragma once
//
// Created by Ugo Varetto on 9/9/16.
//
// libwebsockets based server:
// - forward received data to user-provided callback
// - send the same data to all connected client
// - offer option to reuse memory by storing the consumed data buffer into
//   a memory pool accessible from client code
//
#include <string>
#include <vector>
#include <libwebsockets.h>
#include <mutex>
#include <cstring>
#include <chrono>
#include <thread>
#include <deque>
#include <utility>
#include <memory>

//Note: use libev if possible

///Send queue entry id, used to track which data is sent to each connected
///client.
using ID = size_t;
///Cliend id.
using ClientId = const void*;

inline bool ValidID(ID id) { return id != 0; }

///Counted synchronized queue: remove entry from queue only if all the
///clients have consumed the entry.
template < typename T >
class CountSyncQueue {
public:
    ///Constructor: initialize client counter and number of times Pop() was
    ///called
    CountSyncQueue() : cnt_(0), popped_(0) {}
    void Push(const T& v) {
        std::lock_guard< std::mutex > l(mutex_);
        queue_.push_back(std::pair< T, ID >(v, NextID()));
    }
    ///Return data with id different from passed id.
    ///Clients shall store the last id returned from this method and pass it
    ///back when needing data
    std::pair< T, ID > Pop(ID id) {
        std::lock_guard< std::mutex > l(mutex_);
        if(queue_.empty()) return std::make_pair(T(), ID(0));
        if(queue_.front().second == id) return std::make_pair(T(), ID(0));
        ++popped_;
        if(popped_ < cnt_) return queue_.front();
        else {
            std::pair< T, ID > d = std::move(queue_.front());
            queue_.pop_front();
            popped_ = 0;
            return d;
        }
    }
    ///Register new client by increasing the number of connected clients
    void Inc() {
        std::lock_guard< std::mutex > l(mutex_);
        ++cnt_;
    }
    ///Remove client
    void Dec() {
        std::lock_guard< std::mutex > l(mutex_);
        --cnt_;
        assert(cnt_ >= 0);
    }
private:
    ///Return next id, all valid ids are different from zero
    int NextID() {
        ++id_;
        if(id_ == 0) ++id_;
        return id_;
    }
private:
    std::deque< std::pair< T, ID > > queue_;
    std::mutex mutex_;
    int cnt_;
    int popped_;
    std::atomic< ID > id_;
};

///Websocket communication status, \c SEND not currently used
enum STATE {SEND, RECV, CONNECT, DISCONNECT};
///Websockets server
/// \tparam CBackT callback invoked at connection, disconnection and receive
/// time
///Data is sent to all connected clients through the Push method
template < typename CBackT >
class WSocketQServer {
public:
    WSocketQServer() = delete;
    WSocketQServer(const WSocketQServer&) = delete;
    WSocketQServer(WSocketQServer&&) = delete;
    WSocketQServer(const std::string& protocolName,
                   int timeout,
                   int port,
                   const CBackT& cb,
                   bool recycleMemory,
                   size_t recvBufferSize = 0x1000,
                   int frameTime = 30) //ms
        : stop(false), cback(cb), frameTime_(frameTime),
          connectedClients_(0), recycleMemory_(recycleMemory) {
        Init(protocolName, timeout, port, recvBufferSize);
    }
    ~WSocketQServer() {
        Stop();
    }
    ///Push data into send queue.
    /// \param d data
    /// \param prePadded \c true if data is already prepadded with \c LWS_PRE
    /// bytes, \c false otherwise
    void Push(const std::vector< unsigned char >& d, bool prePadded) {
        if(prePadded) {
            queue_.Push(std::shared_ptr< std::vector< unsigned char > >
                            (new std::vector< unsigned  char >(d)));
        } else {
            std::vector< unsigned char >* v
                = new std::vector< unsigned char >(LWS_PRE + d.size());
            memmove(v->data() + LWS_PRE, d.data(), d.size());
            queue_.Push(std::shared_ptr< std::vector< unsigned char > >(v));
        }
    }
    ///Push prepadded buffer stored into a \c shared_ptr
    ///This is the preferred way of passing data to the object since internally
    ///only \c shared_ptr objects are used.
    void PushPrePaddedPtr(std::shared_ptr< std::vector< unsigned char > > p) {
        queue_.Push(p);
    }
    ///Return \c shared_ptr pointing to an \c std::vector of the requested size.
    ///The returned object is picked from a pool of objects received through
    ///the Push method or a new one is created if the pool is empty.
    ///Data is already pre-padded and client code shall write at PrePaddingSize
    ///offset.
    ///Using this method to retrieve a buffer allows to avoid unnecessary memory
    ///allocations since the same memory buffer is reused multiple times.
    ///Reusing memory buffers is enabled only when the object is constructed
    ///with a \c recycleMemory flag set to true.
    std::shared_ptr< std::vector< unsigned char > >
    GetConsumedPaddedPtr(size_t sz) {
        std::lock_guard< std::mutex > l(consumedQueueMutex_);
        if(consumedQueue_.empty())
            return std::shared_ptr< std::vector< unsigned char > >(
                new std::vector< unsigned char >(sz)
            );
        std::shared_ptr< std::vector< unsigned char > > p
            = consumedQueue_.front();
        consumedQueue_.pop_front();
        p->resize(sz);
        return p;
    }
    ///Returns \c true if the pool of consumed buffer is empty.
    bool ConsumedQueueEmpty() {
        std::lock_guard< std::mutex > l(consumedQueueMutex_);
        const bool e = consumedQueue_.empty();
        return e;
    }
    ///Minimum time in milliseconds between subsequent sends.
    int FrameTime() const { return frameTime_; }
    ///Number of connected clients.
    int ConnectedClients() const { return connectedClients_; }
    ///Size of pre-padded region.
    const size_t PrePaddingSize() const { return LWS_PRE; }
private:
    ///Per-session data: created when a client connects, destroyed when
    ///it disconnects.
    struct PerSessionData {
        ///Last id of data read from send queue.
        ID lastId; //id of last data sent
        ///Time of previous send or creation time in case of first send.
        std::chrono::steady_clock::time_point prev;
    };
private:
    ///Add consumed (sent) buffer to memory pool.
    void PushConsumedPaddedPtr(
        std::shared_ptr< std::vector< unsigned char > > p) {
        std::lock_guard< std::mutex > l(consumedQueueMutex_);
        consumedQueue_.push_back(p);
    }
    ///Initialize libwebsockets and start service loop in separate thread
    void Init(const std::string& protocol,
              int timeout,
              int port,
              size_t recvBufferSize) {
        protocols = {
            //if using http put the http handler as the first entry in the table
            {
                protocol.c_str(),	//protocol name
                WSocketQServer::Callback, //callback
                sizeof(PerSessionData),	// per session data size
                recvBufferSize,// , //receive buffer size
                0, //ID
                nullptr
            },
            {
                nullptr, nullptr, 0, 0, 0, nullptr //END
            }
        };
        memset(&info, 0, sizeof info);
        info.port = port;
        info.iface = nullptr;

        info.protocols = &protocols[0];
        info.user = this;

        //warnings if following not set to something
        info.gid = -1;
        info.uid = -1;
        context = lws_create_context(&info);
        wsocketsTask = std::async(std::launch::async, [this, timeout]() {
            while(!this->stop && lws_service(this->context, timeout) >=0);
        });
    }
    ///Stop service loop.
    void Stop() {
        stop = true;
        wsocketsTask.get();
    }
private:
    ///libwebsockets context.
    lws_context* context;
    ///libwebsockets context creation data.
    lws_context_creation_info info;
    ///Send queue, each entry is sent to all connected clients.
    CountSyncQueue< std::shared_ptr< std::vector< unsigned char > > > queue_;
    ///Set to \c true to stop service.
    bool stop;
    ///Service task.
    std::future< void > wsocketsTask;
    ///libwebsockets protocols.
    std::vector< lws_protocols > protocols;
    ///Client-provided callback.
    CBackT cback;
    ///Time in ms between subsequent writes.
    int frameTime_;
    ///Number of connected clients.
    int connectedClients_;
    ///Pool of consumed memory buffer to be reused by client code.
    std::deque< std::shared_ptr< std::vector< unsigned char > > >
        consumedQueue_;
    ///Guard access to \c consumedQueue_ from different threads.
    std::mutex consumedQueueMutex_;
    ///If set to \c true the send buffer is added into the memory pool
    ///to be reused by client code.
    bool recycleMemory_;
private:
    ///libwebsockets callback.
    static int
    Callback(lws *wsi,
             lws_callback_reasons reason,
             void* user,
             void* in,
             size_t len) {
        PerSessionData *pss = reinterpret_cast< PerSessionData* >(user);
        lws_context* context = lws_get_context(wsi);
        WSocketQServer* wso =
            reinterpret_cast< WSocketQServer* >(
                lws_context_user(context));
        switch(reason) {
        case LWS_CALLBACK_SERVER_WRITEABLE: {
            CountSyncQueue< std::shared_ptr< std::vector< unsigned char > > >*
                queue = &wso->queue_;
            std::pair< std::shared_ptr< std::vector< unsigned char > >, ID >
                data = queue->Pop(pss->lastId);
            if(!ValidID(data.second) || data.first->empty()) {
                lws_callback_on_writable(wsi);
                break;
            }
            pss->lastId = data.second;
            lws_write_protocol writeMode = LWS_WRITE_BINARY;
            const int sent =
                lws_write(wsi, data.first->data() + LWS_PRE, data.first->size()
                              - LWS_PRE,
                          writeMode);
            if(wso->recycleMemory_)
                wso->PushConsumedPaddedPtr(data.first);
            if(sent < 0) {
                lwsl_err("ERROR %d writing to socket, hanging up\n", sent);
                return -1;
            }
            if(sent < data.first->size() - LWS_PRE) {
                lwsl_err("Partial write\n");
                return -1;
            }
            const std::chrono::milliseconds d =
                std::chrono::duration_cast< std::chrono::milliseconds >
                    (std::chrono::steady_clock::now() - pss->prev);
            const auto ft =
                std::chrono::milliseconds(wso->FrameTime());
            if(d < ft) std::this_thread::sleep_for(ft - d);
            pss->prev = std::chrono::steady_clock::now();
            lws_callback_on_writable(wsi);
        }
            break;
        case LWS_CALLBACK_RECEIVE:
            wso->cback(RECV, user, in, len,
                       lws_is_final_fragment(wsi),
                       lws_frame_is_binary(wsi));
            break;
        case LWS_CALLBACK_ESTABLISHED:
            if(pss->lastId == 0) {
                pss->prev = std::chrono::steady_clock::now();
            }
            wso->queue_.Inc();
            wso->connectedClients_++;
            wso->cback(CONNECT, user, nullptr, 0, 0, 0);
            lws_callback_on_writable(wsi);
            break;
        case LWS_CALLBACK_CLOSED:
            wso->queue_.Dec();
            wso->connectedClients_--;
            wso->cback(DISCONNECT, user, nullptr, 0, 0, 0);
            break;
        default:
            break;
        }

        return 0;
    }
};