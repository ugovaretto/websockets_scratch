#pragma once
//
// Created by Ugo Varetto on 9/9/16.
//
#include <string>
#include <vector>
#include <libwebsockets.h>
#include <mutex>
#include <cstring>
#include <chrono>
#include <thread>

//Note: use libev if possible

template < typename RecvCBackT >
class WSocketServer {
public:
    WSocketServer() = delete;
    WSocketServer(const WSocketServer&) = delete;
    WSocketServer(WSocketServer&&) = delete;
    WSocketServer(const std::string& protocolName,
                  int timeout,
                  int port,
                  const RecvCBackT& cb,
                  int frameTime = 30)
        : stop(false), cback(cb), data(frameTime) {
        Init(protocolName, timeout, port);
    }
    ~WSocketServer() {
        Stop();
    }
    void Push(const std::vector< unsigned char >& d, bool prePadded) {
        data.Set(d, prePadded);
    }
private:
    class Data {
    public:
        Data(int frameTime) : id_(0), frameTime_(frameTime) /*ms*/ {}
        size_t ID() const {
            return id_;
        }
        void Set(const std::vector< unsigned char >& v, bool prePadded) {
            std::lock_guard< std::mutex > l(guard_);
            data_ = v;
            ++id_;
            if(!id_) ++id_; //0 means empty
            prePadded_ = prePadded;
        }
        void Get(std::vector< unsigned char >& v) const {
            std::lock_guard< std::mutex > l(guard_);
            if(prePadded_) {
                v = data_;
            } else {
                v.resize(LWS_PRE + data_.size());
                memmove(v.data() + LWS_PRE, data_.data(), data_.size());
            }
        }
        bool Empty() const { return id_ == 0; }
        const std::chrono::milliseconds FrameTime() const { return frameTime_; }
    private:
        size_t id_;
        std::vector< unsigned char > data_;
        std::chrono::milliseconds frameTime_;
        mutable std::mutex guard_;
        bool prePadded_;
    };

    struct PerSessionData {
        std::vector< unsigned char > buf;
        size_t lastId; //id of last data sent
        Data* data;    //reference to data
        std::chrono::steady_clock::time_point prev;
    };
private:
    void Init(const std::string& protocol,
              int timeout,
              int port) {
        protocols = {
            //if using http put the http handler as the first entry in the table
            {
                protocol.c_str(),	//protocol name
                WSocketServer::Callback, //callback
                sizeof(PerSessionData),	// per session data size
                0x1000, //receive buffer size
                0, //ID
                nullptr
            },
            {
                nullptr, nullptr, 0, 0, 0, nullptr //END
            }
        };
        memset(&info, 0, sizeof info);
        info.port = port;
        info.iface = nullptr;

        info.protocols = &protocols[0];
        info.user = this;

        //warnings if following not set to something
        info.gid = -1;
        info.uid = -1;
        context = lws_create_context(&info);
        wsocketsTask = std::async(std::launch::async, [this, timeout]() {
            while(!this->stop && lws_service(this->context, timeout) >=0);
        });
    }
    void Stop() {
        stop = true;
        wsocketsTask.get();
    }
private:
    lws_context* context;
    lws_context_creation_info info;
    Data data;
    bool stop;
    std::future< void > wsocketsTask;
    std::vector< lws_protocols > protocols;
    RecvCBackT cback;
private:
    static int
    Callback(lws* wsi,
             lws_callback_reasons reason,
             void* user,
             void* in,
             size_t len) {

        PerSessionData *pss = reinterpret_cast< PerSessionData* >(user);
        lws_context* context = lws_get_context(wsi);
        WSocketServer* wso =
            reinterpret_cast< WSocketServer* >(
                lws_context_user(context));
        switch(reason) {
        case LWS_CALLBACK_SERVER_WRITEABLE: {
            Data* data = &wso->data;
            pss->data = data;
            if(!pss->data
                || pss->data->Empty()
                || pss->lastId == pss->data->ID()) {
                lws_callback_on_writable(wsi);
                break;
            }
            pss->data->Get(pss->buf);
            pss->lastId = pss->data->ID();
            lws_write_protocol writeMode = LWS_WRITE_BINARY;
            const int sent =
                lws_write(wsi, &pss->buf[LWS_PRE], pss->buf.size() - LWS_PRE,
                          writeMode);
            if(sent < 0) {
                lwsl_err("ERROR %d writing to socket, hanging up\n", sent);
                return -1;
            }
            if(sent < pss->buf.size() - LWS_PRE) {
                lwsl_err("Partial write\n");
                return -1;
            }
            const std::chrono::milliseconds d =
                std::chrono::duration_cast< std::chrono::milliseconds >
                    (std::chrono::steady_clock::now() - pss->prev);
            const auto ft = data->FrameTime();
            if(d < ft) std::this_thread::sleep_for(ft - d);
            pss->prev = std::chrono::steady_clock::now();
            lws_callback_on_writable(wsi);
        }
            break;
        case LWS_CALLBACK_RECEIVE:
            wso->cback(in, len,
                       lws_is_final_fragment(wsi),
                       lws_frame_is_binary(wsi));
            break;
        case LWS_CALLBACK_ESTABLISHED:
            if(pss->lastId == 0) {
                new (&pss->buf) std::vector< unsigned char >;
                pss->prev = std::chrono::steady_clock::now();
            }
            lws_callback_on_writable(wsi);
            break;
        case LWS_CALLBACK_CLOSED:
            pss->buf.~vector< unsigned char >();
            wso->cback(nullptr, 0, 0, 0);
            break;
        default:
            break;
        }

        return 0;
    }
};