// Author: Ugo Varetto
//
// websockets server: stream images to client and receive and print events
//

#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <limits>
#include <string>
#include <vector>
#include <fstream>
#include <thread>
#include <future>
#include <stdexcept>
#include <map>

#include "wserr.h"
#include "WSocketMServer.h"

enum EventId { MOUSE_DOWN = 1, MOUSE_UP = 2, MOUSE_MOVE = 3, KEY = 4,
               MOUSE_WHEEL = 5, RESIZE = 6, TEXT = 7};

namespace {
volatile bool forceExit = false;
}

void forceQuit(int) {
    forceExit = true;
}

#include <map>
using namespace std;

string EventToStr(EventId id) {
    static map< EventId, string > e2s = {
        {MOUSE_DOWN, "mouse down"},
        {MOUSE_UP,   "mouse up"},
        {MOUSE_MOVE, "mouse move"},
        {KEY,        "key"},
        {MOUSE_WHEEL,"mouse wheel"},
        {RESIZE,     "resize window"},
        {TEXT,       "text"}
    };
    if(id < MOUSE_DOWN || id > TEXT)
        throw std::range_error("wrong event id");
    return e2s[id];
}

size_t FileSize(const string& fname) {
    ifstream file(fname);
    assert(file);
    file.ignore(std::numeric_limits< std::streamsize >::max());
    std::streamsize length = file.gcount();
    file.clear();   //  Since ignore will have set eof.
    return length;
}

int main(int argc, char** argv) {

    //--------------------------------------------------------------------------
#ifdef SERIALIZE_IMAGE_SIZE
    if(argc != 6) {
        cerr << "usage: " << argv[0] << " <prefix> <num images> <suffix> <client window width> <client window height>"
             << endl;
        cerr << "E.g.: '>image_stream frame 10 .jpg 1920 1080' loops over "
             << "frame0.jpg to frame9.jpg and requests to the client a 1920x1080 window size"
             << endl;
        return EXIT_FAILURE;
    }
    const int width = stoi(argv[4]);
    const int height = stoi(argv[5]);
    if(width <= 0 || height <= 0) {
        cerr << "Invalid window size" << endl;
        return EXIT_FAILURE;
    }
#else
    if(argc != 4) {
        cerr << "usage: " << argv[0] << " <prefix> <num images> <suffix>"
             << endl;
        cerr << "E.g.: '>image_stream frame 10 .jpg' loops over "
             << "frame0.jpg to frame9.jpg"
             << endl;
        return EXIT_FAILURE;
    }
#endif
    const string prefix = argv[1];
    const int numfiles = int(stol(argv[2]));
    const string suffix = argv[3];
    const int frameDigits = int(floor(log10(numfiles)));
    using Image = vector< unsigned char >;
    using ImageArray = vector< shared_ptr< Image > >;
    ImageArray imgs;
    for(int i = 0; i != numfiles; ++i) {
        const size_t padding =
            max(0, frameDigits - int(floor(log10(i + 1))) - 1);
        assert(padding >= 0);
        const string fname =
            prefix
            + string(padding, '0')
            + to_string(i)
            + suffix;
        ifstream is(fname, ios::binary);
        assert(is);
        const size_t fileSize = FileSize(fname);
        assert(fileSize);
#ifdef SERIALIZE_IMAGE_SIZE
        //To read image + width + height from JavaScript:
        ////// from streaming_client_img_info.html
        //extract width and height from binary buffer and update window size accordingly
        //var dv = new DataView(e.data.slice(0, 8));
        //W = dv.getInt32(0, true /*LITTLE ENDIAN*/);
        //H = dv.getInt32(4, true /*LITTLE_ENDIAN*/);
        //this.width = W;
        //this.height = H;
        //imageWidth.text(W);
        //imageHeight.text(H);
        ////extract jpeg image from stream and render on screen
        //blob = new Blob([e.data.slice(8)], {type: MIMETYPE});
        //imageUrl = urlCreator.createObjectURL(blob);
        //img.src = imageUrl;
        //////
        shared_ptr< Image > img(new Image(fileSize + LWS_PRE + 2 * sizeof(int)));
        is.read(reinterpret_cast< char* >(img->data() + LWS_PRE + 2 * sizeof(int)), fileSize);
        memmove(img->data() + LWS_PRE, &width, sizeof(int));
        memmove(img->data() + LWS_PRE + sizeof(width), &height, sizeof(int));
#else
        shared_ptr< Image > img(new Image(fileSize + LWS_PRE));
        is.read(reinterpret_cast< char* >(img->data() + LWS_PRE), fileSize);
#endif
        imgs.push_back(img);

    }
    auto cback = [](STATE s, ClientId cid, void* in, int len, int, int) {
        if(s == CONNECT) {
            cout << cid << "> Connected" << endl;
            return;
        } else if(s == DISCONNECT) {
            cout << cid << "> Disconnected" << endl;
            return;
        }
        const int* p = reinterpret_cast< const int* >(in);
        cout << cid << "> " << EventToStr(EventId(*p));
        switch(EventId(*p)) {
        case MOUSE_DOWN:
        case MOUSE_UP:
        case MOUSE_MOVE: {
            const int* pos = ++p;
            cout << " x: " << *pos << " y: " << *(pos + 1) << endl;
        }
            break;
        case RESIZE: {
            const int* pos = ++p;
            cout << " width: " << *pos << " height: " << *(pos + 1) << endl;
        }
            break;
        case KEY: {
            const int* pos = ++p;
            cout << " key: " << char(*pos);
            const int modifier = *(++pos);
            if(0x1   & modifier) cout << " Alt";
            if(0x10  & modifier) cout << " Ctrl";
            if(0x100 & modifier) cout << " Shift";
            cout << endl;
        }
            break;
        case MOUSE_WHEEL: {
            const int* pos = ++p;
            cout << " wheel: " << *pos << endl;
        }
            break;
        default:
            break;
        }
    };

    //comment following line to enable log to stdout/err
    InitWSThrowErrorHandler();
    const int sendInterval = 0;
    try {
        WSocketMServer<decltype(cback)> wso("image-stream", //protocol name
                                            1000, //timeout: will spend this time to process
                //websocket traffic, the higher the better
                                            7681, //port
                                            cback, //callback
                                            false, //recycle memory
                                            0x1000, //input buffer size
                                            sendInterval);  //min interval between sends in ms
        signal(SIGINT, forceQuit);
        int count = 0;
        while (!forceExit) {
            if(sendInterval > 0)
                this_thread::sleep_for(chrono::milliseconds(sendInterval));
            if (wso.ConnectedClients() == 0) continue;
            const bool prePaddingOption = true;
            wso.PushPrePaddedPtr(imgs[count % imgs.size()]);
            ++count;
        }
    } catch(const exception& e) {
        cerr << e.what() << endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//sample javascript code, includes event handlers - remove or include
//event-handlers.js
/*******************************************************************************
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <script type="text/javascript" src="event-handlers.js">
    </script>
<script src=
    "http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function() {
    //CHANGE ACCORDINGLY
    var MIMETYPE = "image/jpeg";
    //var MIMETYPE = "image/webp";
    var websocket;
    window.WebSocket = window.WebSocket || window.MozWebSocket;
    var out = $("[name='output']");
    var bytes = $("[name='bytes']");
    var imageWidth  = $("[name='width']");
    var imageHeight  = $("[name='height']");
    var m  = $("[name='min']");
    var M  = $("[name='max']");
    var D  = $("[name='delta']");
    var img = document.querySelector( "#photo" );
    var blob = new Blob( [], { type: MIMETYPE } );
    var urlCreator = window.URL || window.webkitURL;
    var imageUrl;
    var b = document.querySelector( "body" );
    var W, H;
    var minSize = 0x100000000;
    var maxSize = 0;

    var proto = location.protocol;
    var hostname = "localhost";
    if(proto != "file:") hostname = location.hostname;
    var WSURL = "ws://" + hostname + ":7681";
    websocket = new WebSocket(WSURL, 'image-stream');
    websocket.binaryType = "arraybuffer";

    function resizeImage() {
        sendResizeEvent(window.innerWidth, window.innerHeight);
    }

    window.addEventListener('resize',resizeImage, true);

    while(eventScriptLoaded == undefined);
    initEvents(img, websocket);

    websocket.onopen = function () {
        $('h1').css('color', 'green');
    };

    websocket.onerror = function (e) {
        $('h1').css('color', 'red');
        console.log(e);
    };
    var start = performance.now();
    var elapsed = 0;
    var frames = 0;
    img.onload = function() {
        W = this.width;
        H = this.height;
        if(frames == 1) {
            resizeImage();
        }
    }
    var size;
    var dSize;

    websocket.onmessage = function (e) {
        if(imageUrl)
            urlCreator.revokeObjectURL(imageUrl);
        frames++;
        elapsed = performance.now() - start;
        //out.text(e.data.byteLength);
        out.text((1000 * frames / elapsed).toFixed(0));
        size = e.data.byteLength;
        bytes.text(size);
        if(size < minSize) minSize = size;
        if(size > maxSize) maxSize = size;
        dSize = maxSize - minSize;
        m.text(minSize);
        M.text(maxSize);
        D.text(dSize);
        imageWidth.text(W);
        imageHeight.text(H);
        blob = new Blob( [e.data], { type: MIMETYPE } );
        imageUrl = urlCreator.createObjectURL( blob );
        img.src = imageUrl;
    }

});
</script>
<style>
    body{
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
</head>
<body>
<div><img id="photo" style="pointer-events: none; position: absolute; top:0;left:0; overflow: scroll"/></div>
<div style="color: red; font-size: 200%; position: fixed">
    <h1>WebSockets stream test</h1>
<div>Frame/s: <span name="output"></span></div>
<div>Size: <span name="bytes"></span> bytes</div>
<div>Width: <span name="width"></span></div>
<div>Height: <span name="height"></span></div>
<div>Min size: <span name="min"></span></div>
<div>Max size: <span name="max"></span></div>
<div>Max - min size: <span name="delta"></span></div>
<div><span id="pos"></span></div>
</div>
</body>
</html>
*******************************************************************************/