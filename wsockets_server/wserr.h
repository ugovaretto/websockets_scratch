//
// Created by Ugo Varetto on 5/12/2016.
//
#pragma once

#include <cassert>
#include <stdexcept>
#include <libwebsockets.h>

///Disable logging and convert errors to exceptions
inline void InitWSThrowErrorHandler() {
    lws_set_log_level(int(LLL_ERR),
                      [](int level, const char* line) {
                          assert(level == LLL_ERR);
                          throw std::runtime_error(line);
                      });
}

