#include <cstring>
#include <cassert>
#include <iostream>
#include <mutex>
#include <vector>
#include <libwebsockets.h>

//Note: use libev if possible

using namespace std;

namespace {
volatile bool forceExit = 0; //set by signal handler
}

class Data {
public:
    Data() : id_(0) {}
    size_t ID() const {
        return id_;
    }
    void Set(const vector< unsigned char >& v) {
        lock_guard< mutex > l(guard_);
        data_ = v;
        ++id_;
        if(!id_) ++id_; //0 means empty
    }
    void Get(vector< unsigned char >& v) const {
        lock_guard< mutex > l(guard_);
        v.resize(LWS_PRE + data_.size());
        copy(data_.begin(), data_.end(), v.begin() + LWS_PRE);
    }
    bool Empty() const { return id_ == 0; }
private:
    size_t id_;
    vector< unsigned char > data_;
    mutable mutex guard_;
};


struct PerSessionData {
    vector< unsigned char > buf;
    size_t lastId; //id of last data sent
    Data* data;    //reference to data
};

int
CallbackStream(lws *wsi,
               lws_callback_reasons reason,
               void* user,
               void* in,
               size_t len)
{
    PerSessionData *pss = reinterpret_cast< PerSessionData* >(user);
    lws_context* context = lws_get_context(wsi);

    switch (reason) {
    case LWS_CALLBACK_SERVER_WRITEABLE: {
        Data* data = reinterpret_cast< Data* >(lws_context_user(context));
        pss->data = data;
        if(!pss->data
            || pss->data->Empty()
            || pss->lastId == pss->data->ID()) {
            lws_callback_on_writable(wsi);
            break;
        }
        pss->data->Get(pss->buf);
        pss->lastId = pss->data->ID();
        lws_write_protocol writeMode = LWS_WRITE_BINARY;
        const int sent =
            lws_write(wsi, &pss->buf[LWS_PRE], pss->buf.size() - LWS_PRE,
                      writeMode);
        if(sent < 0) {
            lwsl_err("ERROR %d writing to socket, hanging up\n", sent);
            return -1;
        }
        if(sent < pss->buf.size() - LWS_PRE) {
            lwsl_err("Partial write\n");
            return -1;
        }
        lws_callback_on_writable(wsi);
    }
        break;
    case LWS_CALLBACK_ESTABLISHED:
        if(pss->lastId == 0) {
            new (&pss->buf) vector< unsigned char >;
        }
        lws_callback_on_writable(wsi);
        break;
    case LWS_CALLBACK_CLOSED:
        pss->buf.~vector< unsigned char >();
        break;
    default:
        break;
    }

    return 0;
}



static struct lws_protocols protocols[] = {
    //if using http put the http handler as the first entry in the table
    {
        "stream",	//protocol name
        CallbackStream, //callback
        sizeof(PerSessionData),	// per session data size
        0x1000, //receive buffer size
        0, //ID
        nullptr
    },
    {
       nullptr, nullptr, 0, 0, 0, nullptr //END
    }
};

void sighandler(int) {
    forceExit = 1;
}

int main() {

    lwsl_notice("libwebsockets streaming server\n");

    struct lws_context* context;
    struct lws_context_creation_info info;
    int listenPort = 7681;
    Data data;

    memset(&info, 0, sizeof info);

    info.port = listenPort;
    info.iface = nullptr;
    info.protocols = protocols;
    info.user = &data;

    info.gid = -1;
    info.uid = -1;

    context = lws_create_context(&info);
    if (context == NULL) {
        lwsl_err("libwebsocket init failed\n");
        return -1;
    }

    signal(SIGINT, sighandler);

    size_t count = 0;
    auto fillIt = [&count](vector< unsigned char >& v) {
        string n = to_string(count);
        v.resize(0);
        for(auto c: n) v.push_back(c);
    };
    vector< unsigned char > v;
    fillIt(v);
    data.Set(v);
    while(!forceExit && lws_service(context, 1000) >=0) {
        ++count;
        fillIt(v);
        data.Set(v);
    }

    lws_context_destroy(context);

    lwsl_notice("libwebsockets streaming server exited cleanly\n");

    return 0;
}

//FRAGMENTED PACKETS:
//int n = libwebsocket_write(wsi, &write_buffer[LWS_SEND_BUFFER_PRE_PADDING],
//                           write_len,(libwebsocket_write_protocol)write_mode);
//The write_mode should be set as below:
//
//int write_mode;
//write_mode = LWS_WRITE_BINARY; // single frame, no fragmentation
//write_mode = LWS_WRITE_BINARY | LWS_WRITE_NO_FIN; // first fragment
//write_mode = LWS_WRITE_CONTINUATION | LWS_WRITE_NO_FIN;// all middle fragments
//write_mode = LWS_WRITE_CONTINUATION; // last fragment
//If you need more details, read the fragmentation section of the WebSocket
// RFC: https://tools.ietf.org/html/rfc6455#section-5.4