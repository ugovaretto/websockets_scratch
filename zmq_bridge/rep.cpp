//
// Created by Ugo Varetto on 5/12/2016.
//
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <vector>
#include <zmq.h>

namespace {
    volatile bool forceExit = false;

    void forceQuit(int) {
        forceExit = true;
    }
}


using namespace std;

int main(int, char**) {
    void* ctx = zmq_ctx_new();
    assert(ctx);
    void* s = zmq_socket(ctx, ZMQ_REP);
    assert(s);
    const char* url = "tcp://*:8888";
    if(zmq_bind(s, url)) {
        cerr << "Error binding to " << url << endl;
        return EXIT_FAILURE;
    }
    signal(SIGINT, forceQuit);
    int cnt = 0;
    const int bufsize = 0x1000;
    vector< char > buf(bufsize);
    vector< char > outbuf(bufsize);
    while(!forceExit) {
        buf.resize(bufsize);
        const int n = zmq_recv(s, buf.data(), buf.size(), 0);
        buf.resize(n);
        copy(buf.rbegin(), buf.rend(), outbuf.begin());
        zmq_send(s, outbuf.data(), n, 0);
    }
    zmq_close(s);
    zmq_ctx_destroy(ctx);
    return EXIT_SUCCESS;
}
