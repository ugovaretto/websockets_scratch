/// \warning won't compile

//
// Created by Ugo Varetto on 9/19/16.
//

// Author: Ugo Varetto
//
// websockets server: stream images to client and receive and print events
//

#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <limits>
#include <string>
#include <vector>
#include <fstream>
#include <thread>
#include <future>
#include <stdexcept>
#include <map>
#include <set>
#include <zmq.h>

#include "../wsockets_server/WSocketMServer.h"

namespace {
volatile bool forceExit = false;
}

void forceQuit(int) {
    forceExit = true;
}


enum WS_ZMQ {WS_ZMQ_CONNECT, WS_ZMQ_SEND};
enum WS_ZMQ_SOCKET_TYPE {WS_ZMQ_REQ, WS_ZMQ_SUB, WS_ZMQ_PULL};

//'auto' only works with c++17
template < typename RecvCBackT, typename StopT >
std::function< void () >
ZMQRecvCBack(WSocketMServer< RecvCBackT >* wss,
             ClientId id,
             void* s,
             int timeoutms,
             size_t bufferSize,
             const StopT& stopRequested) {
    return [wss, id, s, timeoutms, bufferSize, stopRequested]() {
        zmq_pollitem_t items[] = {
            {s, 0, ZMQ_POLLIN, 0}};
        while(!stopRequested(id)) {
            zmq_poll(items, 1, timeoutms);
            if(items[0].revents & ZMQ_POLLIN) {
                std::shared_ptr< std::vector< unsigned char > > buf
                    = wss->GetConsumedPaddedPtr(bufferSize);
                const int n = zmq_recv(s,
                                       buf->data() + wss->PrePaddingSize(),
                                       bufferSize, 0);
                wss->PushPrePaddedPtr(buf);
            }
        }
    };
}

template < typename T >
T Parse(void*& p) {
    const T v = *(reinterpret_cast< const T* >(p));
    p = reinterpret_cast< char* >(p) + sizeof(T);
    return v;
}

template < typename T >
std::vector< T > ParseArray(void*& in, size_t numElem) {
    std::vector< T > v(numElem);
    memmove(v.data(), in, numElem * sizeof(T));
    in = reinterpret_cast< char* >(in) + numElem * sizeof(T);
    return v;
}

class ZMQWSocketBridge {
   struct RecvCBack {
       RecvCBack(ZMQWSocketBridge* b) : b_(b) {}
       void operator()(STATE s, ClientId cid, void* in, int len, int, int) {
           if(s == DISCONNECT) {
               b_->Cleanup(cid);
           } else if(s == RECV) {
               if(b_->clientToSocket_.find(cid) == b_->clientToSocket_.end()) {
                   //not found: create socket and add client id -> socket map
                   assert(in);
                   assert(len > sizeof(int));
                   // MSG:
                   // |int - command id|data|
                   // data:
                   // |fixed|[data] or |int - length|chars|[data]
                   const int cmdId = Parse< int >(in);
                   //first message MUST be WS_ZMQ_CONNECT with accompanying URL
                   assert(cmdId == WS_ZMQ_CONNECT);
                   const WS_ZMQ_SOCKET_TYPE stype
                       = Parse< WS_ZMQ_SOCKET_TYPE >(in);
                   const int bufSize = Parse< int >(in);
                   std::vector< char > buf(ParseArray< char >(in, bufSize));
                   buf.push_back('\0');
                   void* sock = nullptr;
                   switch(stype) {
                   case WS_ZMQ_REQ:
                        sock = zmq_socket(b_->ctx_, ZMQ_REQ);
                        zmq_connect(sock, buf.data());
                        break;
                   case WS_ZMQ_SUB:
                        sock = zmq_socket(b_->ctx_, ZMQ_SUB);
                        zmq_connect(sock, buf.data());
                        break;
                   case WS_ZMQ_PULL:
                        sock = zmq_socket(b_->ctx_, ZMQ_SUB);
                        zmq_connect(sock, buf.data());
                   default:
                       break;
                   }
                   if(!sock) throw std::domain_error("Wrong ZMQ socket type");
                   b_->Init(cid, sock);

               } else {
                   zmq_send(b_->clientToSocket_[cid],
                            reinterpret_cast< char* >(in),
                            len, ZMQ_NOBLOCK);
               }
           }
       }
       ZMQWSocketBridge* b_;
   };
public:
    ZMQWSocketBridge(const char* wsProtocolName,
                     int wsTimeout,
                     int wsPort,
                     size_t wsInputBufferSize,
                     int wsSendInterval,
                     int zmqTimeout,
                     size_t zmqBufferSize) :
            wss_(wsProtocolName, wsTimeout, wsPort, RecvCBack(this),
                 false, wsInputBufferSize, wsSendInterval),
     zmqTimeout_(zmqTimeout), zmqBufferSize_(zmqBufferSize) {}
private:
    using Lock = std::lock_guard< std::mutex >;
    bool StopRequested(ClientId id) {
        Lock l(stoppedGuard_);
        return stopped_.find(id) != stopped_.end();
    }
    void Init(ClientId cid, void* s) {
        //start new task that communicates with remote ZMQ server
        Lock l(tasksGuard_);
        zmqRecvTasks_[cid] = std::move(std::async(std::launch::async,
            ZMQRecvCBack(&wss_,
                     cid,
                     s,
                     zmqTimeout_,
                     zmqBufferSize_,
                     [this](ClientId id) {
                         return this->StopRequested(id);
                     }
            )));
    }
    void Cleanup(ClientId cid) {
        {
            Lock l(clientToSocketGuard_);
            if(clientToSocket_.find(cid) == clientToSocket_.end())
                return;
            zmq_close(clientToSocket_[cid]);
            clientToSocket_.erase(clientToSocket_.find(cid));
        }
        {
            Lock l(stoppedGuard_);
            stopped_.erase(cid);
        }
        {
            Lock l(tasksGuard_);
            if(zmqRecvTasks_.find(cid) == zmqRecvTasks_.end())
                return;
            zmqRecvTasks_.erase(zmqRecvTasks_.find(cid));
        }
    }
private:
    WSocketMServer< RecvCBack > wss_;
    int zmqTimeout_;
    size_t zmqBufferSize_;
    std::map< ClientId, void* > clientToSocket_;
    std::mutex clientToSocketGuard_;
    std::set< ClientId > stopped_;
    std::mutex stoppedGuard_;
    std::map< ClientId, std::future< void > > zmqRecvTasks_;
    std::mutex tasksGuard_;
    void* ctx_;
};

using namespace std;

int main(int argc, char** argv) {

    auto z = std::async(std::launch::async,[]() {
        void* ctx = zmq_ctx_new();
        void* s = zmq_socket(ctx, ZMQ_REP);
        zmq_bind(s, "tcp://*:8887");
        vector< char > buf(0x10000);
        while(true) {
            const int rc = zmq_recv(s, buf.data(), buf.size(), 0);
            zmq_send(s, buf.data(), rc, 0);
        }
    });

    ZMQWSocketBridge zws("zmq-req-rep", 10, 7681, 0x1000, 1, 10, 0x1000);

    z.wait();
    return EXIT_SUCCESS;
}