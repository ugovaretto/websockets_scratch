// Author: Ugo Varetto
//
// ZMQ-WebSockets bridge - fixed endpoints:
// * bridge is started and connects to either a PUB or REP ZMQ server
// * PUB: messages start to be streamed to WebSockets client as soon as it connects to the bridge
// * REP: each message received from the client is sent to the REP server as a REQ and the response is
//        sent back to the client
//
// usage: zws <server URL> <PUB|REP> <websockets tcp port> [websockets protocol name]

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <string>
#include <utility>
#include <deque>
#include <mutex>
#include <zmq.h>
#include "../wsockets_server/WSocketMServer.h"

namespace {
    volatile bool forceExit = false;
    void forceQuit(int) {
        forceExit = true;
    }
}

template< typename T >
class SQueue {
public:
    void Push(T&& d) {
        std::lock_guard< std::mutex > l(m_);
        queue_.push_back(std::forward< T >(d));
    }
    T Pop() {
        std::lock_guard< std::mutex > l(m_);
        T d(std::move(queue_.front()));
        queue_.pop_front();
        return d;
    }
    bool Empty() const {
        std::lock_guard< std::mutex > l(m_);
        const bool e = queue_.empty();
        return e;
    }
private:
    std::deque< T > queue_;
    mutable std::mutex m_;

};

using namespace std;


int main(int argc, char** argv) {

    //parse command line: <- ZMQ server url, pub or sub, websockets port
    if(argc != 4) {
        cerr << "usage: " << argv[0] << " <zmq server url> <PUB|REP> <websockets service port>" << endl;
        return EXIT_FAILURE;
    }
    const string URL = argv[1];
    string PROTO(argv[2]);
    transform(argv[2], argv[2] + strlen(argv[2]), begin(PROTO), [](char c) { return toupper(c);});
    if(PROTO != "PUB" && PROTO != "REP") {
        cerr << "Invalid protocol, only PUB or REP are supported for ZMQ service" << endl;
        return EXIT_FAILURE;
    }
    const int wsport = stoi(argv[3]);
    //connect to server, exit on failure
    void* ctx = zmq_ctx_new();
    assert(ctx);
    void* s = PROTO == "REP" ? zmq_socket(ctx, ZMQ_REQ) : zmq_socket(ctx, ZMQ_SUB);
    if(PROTO == "PUB") {
        zmq_setsockopt (s, ZMQ_SUBSCRIBE, 0, 0);
    }
    assert(s);
    if(zmq_connect(s, argv[1])) {
        cerr << "Error connecting to " << argv[1] << endl;
        return EXIT_FAILURE;
    }
    //start websockets service
    SQueue< vector< unsigned char > > inqueue;

    //callback: add received message into queue
    auto cback = [&inqueue](STATE s, ClientId cid, void* in, int len, int, int) {
        if(s == CONNECT) cout << cid << " connected" << endl;
        if(s == RECV) {
            const unsigned char *begin =
                    reinterpret_cast< const unsigned char * >(in);
            const unsigned char *end =
                    reinterpret_cast< const unsigned char * >(in) + len;
            inqueue.Push(vector<unsigned char>(begin, end));
        }
    };
    //intercept SIGINT
    signal(SIGINT, forceQuit);

    WSocketMServer< decltype(cback) > wss("zmq-bridge", //protocol name
                                        1000, //timeout: will spend this time to process
                                              //websocket traffic, the higher the better
                                        wsport, //port
                                        cback, //callback
                                        true, //recycle memory
                                        0x10000, //input buffer size
                                        29);  //min interval between sends in ms

    //start zmq bridge service
    const int bufferSize = 0x10000;
    vector< unsigned char > buf(bufferSize);
    zmq_pollitem_t items[] = {
            {s, 0, ZMQ_POLLIN, 0}};
    while(!forceExit) {
        if(!wss.ConnectedClients()) continue;
        if(PROTO == "REP" && !inqueue.Empty()) {
            vector< unsigned char > b(inqueue.Pop());
            if(zmq_send(s, b.data(), b.size(), 0) < 0) {
                cerr << "Cannot send data to ZMQ server" << endl;
                return EXIT_FAILURE;
            }
        }
        zmq_poll(items, 1, 0);
        if(items[0].revents & ZMQ_POLLIN) {
            std::shared_ptr< std::vector< unsigned char > > buf
                    = wss.GetConsumedPaddedPtr(bufferSize);

            const int n = zmq_recv(s,
                                   buf->data() + wss.PrePaddingSize(),
                                   bufferSize, 0);
            if(n < 0) {
                cerr << "Error receiving data from ZMQ server" << endl;
                return EXIT_FAILURE;
            }
            wss.PushPrePaddedPtr(buf);
        }
    }
    //cleanup upon Ctrl-C
    zmq_close(s);
    zmq_ctx_destroy(ctx);
    return EXIT_SUCCESS;
}