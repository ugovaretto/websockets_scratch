//
// Created by Ugo Varetto on 5/12/2016.
//
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <zmq.h>

namespace {
    volatile bool forceExit = false;

    void forceQuit(int) {
        forceExit = true;
    }
}


using namespace std;

int main(int, char**) {

    void* ctx = zmq_ctx_new();
    assert(ctx);
    void* s = zmq_socket(ctx, ZMQ_PUB);
    assert(s);
    const char* url = "tcp://*:8888";
    if(zmq_bind(s, url)) {
        cerr << "Error binding to " << url << endl;
        return EXIT_FAILURE;
    }
    signal(SIGINT, forceQuit);
    int cnt = 0;
    while(!forceExit) {
        this_thread::sleep_for(chrono::seconds(1));
        const string n = to_string(cnt++);
        zmq_send(s, n.c_str(), n.size(), 0);
        cout << "Sent " << n << endl;
    }
    zmq_close(s);
    zmq_ctx_destroy(ctx);
    return EXIT_SUCCESS;
}
